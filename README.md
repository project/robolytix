# robolytix

Installation steps:
* Sign up to https://www.robolytix.com/ and create a process there
* Install Robolytix module
* Go to /admin/config/system/robolytix and configure the module
