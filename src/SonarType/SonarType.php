<?php
declare(strict_types = 1);

namespace Drupal\robolytix\SonarType;

class SonarType
{
  const CREATE = 'create';
  const UPDATE = 'update';
  const DELETE = 'delete';
  const MODERATION_CREATE = 'moderation_create';
  const MODERATION_UPDATE = 'moderation_update';
  const MODERATION_DELETE = 'moderation_delete';
}
