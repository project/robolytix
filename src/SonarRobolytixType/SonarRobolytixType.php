<?php
declare(strict_types = 1);

namespace Drupal\robolytix\SonarRobolytixType;

class SonarRobolytixType
{
  const START = 'start';
  const CONTINUOUS = 'continuous';
  const END = 'end';
  const ERROR = 'error';
}
