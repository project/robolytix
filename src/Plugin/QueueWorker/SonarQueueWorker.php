<?php
declare(strict_types = 1);

namespace Drupal\robolytix\Plugin\QueueWorker;
use Drupal\Core\Annotation\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\robolytix\Event\SonarEvent;
use Drupal\robolytix\Exception\QueueWorkerException;

/**
 * Processes Sonar Tasks.
 *
 * @QueueWorker(
 *   id = "sonar_queue",
 *   title = @Translation("Sonar task worker: sonar queue"),
 *   cron = {"time" = 60}
 * )
 */
class SonarQueueWorker  extends QueueWorkerBase
{
    /**
     * {@inheritdoc}
     */
    public function processItem($data) {
        $robolytixApi = \Drupal::service('robolytix.robolytix_api');
        if ($data instanceof SonarEvent) {
          $result = $robolytixApi->postMessageAction($data);
          if ($result === null or $result->getStatusCode() >= 300) {
            throw new QueueWorkerException('Queue item could not be processed: ' . print_r($data, true));
          }
        }
    }
}
