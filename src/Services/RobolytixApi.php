<?php
declare(strict_types = 1);

namespace Drupal\robolytix\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\robolytix\Event\SonarEvent;
use Drupal\robolytix\ProcessType\ProcessType;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RobolytixApi implements ContainerInjectionInterface
{
  /**
   * @var Client $client
   */
  protected $client;

  /**
   * @var ConfigFactoryInterface $configFactory
   */
  protected $configFactory;

  /**
   * @var QueueFactory $queue
   */
  protected $queue;

  /**
   * Class constructor.
   * @param Client $client
   * @param ConfigFactoryInterface $configFactory
   * @param QueueFactory $queue
   */
  public function __construct(Client $client, ConfigFactoryInterface $configFactory, QueueFactory $queue)
  {
    $this->client = $client;
    $this->configFactory = $configFactory;
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('queue')
    );
  }

  /**
   * Post a ping to the Robolytix API.
   * @param string|null $apiKey
   * @return ResponseInterface|null
   */
  public function pingApi(string $apiKey = null): ?ResponseInterface
  {
    $apiKey = $this->getApiKey($apiKey);
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $apiKey
      ]
    ];
    try {
      return $this->client->request('GET', 'https://api.robolytix.com/v1/ping', $options);
    } catch (GuzzleException $e) {
      \Drupal::logger('robolytix')->notice($e->getMessage());
      return null;
    }
  }

  /**
   * Post a message to the Robolytix API.
   * @param SonarEvent $sonarEvent
   * @param string|null $apiKey
   * @return ResponseInterface|null
   */
  public function postMessageAction(SonarEvent $sonarEvent, string $apiKey = null): ?ResponseInterface
  {
    $apiKey = $this->getApiKey($apiKey);
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $apiKey
      ],
      'body' => json_encode($sonarEvent->getBody())
    ];
    try {
      return $this->client->request('POST', 'https://api.robolytix.com/v1/messages', $options);
    } catch (GuzzleException $e) {
      \Drupal::logger('robolytix')->notice($e->getMessage());
      return null;
    }
  }

  /**
   * Post messages to the Robolytix API.
   * @param SonarEvent[] $body
   * @param string|null $apiKey
   * @return ResponseInterface|null
   */
  public function postMessagesBulkAction(array $body, string $apiKey = null): ?ResponseInterface
  {
    $apiKey = $this->getApiKey($apiKey);
    $options = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => $apiKey
      ],
      'body' => json_encode($body)
    ];
    try {
      return $this->client->request('POST', 'https://api.robolytix.com/v1/messages/bulk', $options);
    } catch (GuzzleException $e) {
      \Drupal::logger('robolytix')->notice($e->getMessage());
      return null;
    }
  }

  /**
   * Get aa API key from the Robolytix setting if the key is not defined in the parameter.
   * @param string|null $apiKey
   * @return string|null
   */
  private function getApiKey(?string $apiKey): ?string
  {
    $config = $this->configFactory->get('robolytix.settings');
    $robolytix_enable = $config->get('robolytix_enable');
    if($robolytix_enable and !$apiKey) {
      $apiKey = $config->get('robolytix_auth_token');
    }
    return $apiKey;
  }

  /**
   * Get a process type from the Robolytix setting.
   */
  protected function getProcessType(): string
  {
    return $this->configFactory->get('robolytix.settings')->get('robolytix_process_type');
  }

  /**
   * Is the Robolytix Integration enabled?
   */
  protected function isRobolytixEnabled(): bool
  {
    return $this->configFactory->get('robolytix.settings')->get('robolytix_enable');
  }

  /**
   * Add the item to the Sonar queue.
   * @param SonarEvent $event
   */
  protected function addItemToSonarQueue(SonarEvent $event): void
  {
    $queue = $this->queue->get('sonar_queue');
    $queue->createQueue();
    $queue->createItem($event);
  }

  /**
   * Process the sonar based on setting (just now or add the item to the queue).
   * @param SonarEvent $event
   */
  public function processSonar(SonarEvent $event): void
  {
    if(!$this->isRobolytixEnabled()) {
      return;
    }
    $processType = $this->getProcessType();
    if (ProcessType::PROCESS_IN_QUEUE === $processType) {
      $this->addItemToSonarQueue($event);
    }
    elseif (ProcessType::PROCESS_NOW === $processType) {
      $result = $this->postMessageAction($event);
      if ($result === null or $result->getStatusCode() >= 300) {
        \Drupal::logger('robolytix')->notice('Event could no be saved: ' . print_r($event, true));
      }
    }
    else {
      \Drupal::logger('robolytix')->error('Missing process type');
    }
  }
}
