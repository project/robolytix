<?php
declare(strict_types = 1);

namespace Drupal\robolytix\Services;


use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\node\NodeInterface;
use Drupal\robolytix\Event\SonarEvent;
use Drupal\robolytix\SonarRobolytixType\SonarRobolytixType;
use Drupal\robolytix\SonarType\SonarType;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RobolytixCreateEvent implements ContainerInjectionInterface
{
  /**
   * @var ConfigFactoryInterface $configFactory
   */
  protected $configFactory;

  /**
   * Class constructor.
   * @param ConfigFactoryInterface $configFactory
   */
  public function __construct(ConfigFactoryInterface $configFactory)
  {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * @param NodeInterface $node
   * @param array $actions
   * @return SonarEvent[]
   */
  public function createEvents(NodeInterface $node, array $actions): array
  {
    $sonarEvents = [];
    $config = $this->configFactory->get('robolytix.settings');
    $sonarConfig = $config->get('robolytix_sonar_ids');

    if (is_array($sonarConfig)) {
      foreach ($sonarConfig as $sonarId => $sonarValues) {
        if ($sonarValues['temp_sonar_deleted'] or $sonarValues['temp_sonar_added']) {
          continue;
        }
        if ($node->getType() == $sonarValues['sonar_entity']) {
          $actionIntersection = array_intersect($actions, $sonarValues['sonar_actions']);
          foreach ($actionIntersection as $actionApplied) {
            // Sonar type
            if ($node->isPublished() or in_array(SonarType::DELETE, $actions)){
              $sonarType = SonarRobolytixType::END;
            } elseif (in_array(SonarType::CREATE, $actions)) {
              $sonarType = SonarRobolytixType::START;
            } else {
              $sonarType = SonarRobolytixType::CONTINUOUS;
            }
            // Sonar name
            if(in_array($actionApplied, [SonarType::MODERATION_UPDATE, SonarType::MODERATION_CREATE, SonarType::MODERATION_DELETE]) and $node->hasField('moderation_state')) {
              $sonarName = $node->get('moderation_state')->getString() . ': ' . $actionApplied;
            } else {
              $sonarName = $actionApplied;
            }
            // Messages
            $message = [];
            foreach ($sonarValues['sonar_fields'] as $field => $value) {
              if($value and $node->hasField($field)) {
                $message[$field] = substr($node->get($field)->getString(), 0, 255);
              }
            }
            // New event
            $sonarEvent = new SonarEvent($sonarValues['sonar_process_id'], $sonarName, $sonarType, $node->id(), $message);
            $sonarEvents[] = $sonarEvent;
          }
        }
      }
    }
    return $sonarEvents;
  }
}
