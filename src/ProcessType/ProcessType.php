<?php
declare(strict_types = 1);

namespace Drupal\robolytix\ProcessType;

class ProcessType
{
  const PROCESS_IN_QUEUE = 'process in queue';
  const PROCESS_NOW = 'process now';
}
