<?php
declare(strict_types = 1);

namespace Drupal\robolytix\Event;


final class SonarEvent
{
  /**
   * @var string $processId
   */
  private $processId;

  /**
   * @var string $name
   */
  private $name;

  /**
   * @var string $type
   */
  private $type;

  /**
   * @var ?string $runId
   */
  private $runId;

  /**
   * @var ?array $message
   */
  private $message;

  /**
   * @var ?string $service
   */
  private $service;

  /**
   * @var ?\DateTime $created
   */
  private $created;

  /**
   * Class constructor.
   * @param string $processId
   * @param string $name
   * @param string $type
   * @param string|null $runId
   * @param array|null $message
   * @param string|null $service
   */
  public function __construct(string $processId,
                              string $name,
                              string $type,
                              string $runId = null,
                              array $message = null,
                              string $service = null
  )
  {
    $this->processId = $processId;
    $this->name = $name;
    $this->type = $type;
    $this->runId = $runId;
    $this->message = $message;
    $this->service = $service;
    $this->setCreated();
  }

  /**
   * @return string
   */
  public function getProcessId(): string
  {
    return $this->processId;
  }

  /**
   * @param string $processId
   */
  public function setProcessId(string $processId): self
  {
    $this->processId = $processId;

    return $this;
  }

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): self
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return string
   */
  public function getType(): string
  {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType(string $type): self
  {
    $this->type = $type;

    return $this;
  }

  /**
   * @return string
   */
  public function getRunId(): ?string
  {
    return $this->runId;
  }

  /**
   * @param string $runId
   */
  public function setRunId(string $runId): self
  {
    $this->runId = $runId;

    return $this;
  }

  /**
   * @return array
   */
  public function getMessage(): ?array
  {
    return $this->message;
  }

  /**
   * @param array $message
   */
  public function setMessage(array $message): self
  {
    $this->message = $message;

    return $this;
  }

  /**
   * @return string
   */
  public function getService(): ?string
  {
    return $this->service;
  }

  /**
   * @param string $service
   */
  public function setService(string $service): self
  {
    $this->service = $service;

    return $this;
  }

  /**
   * @return string
   */
  public function getCreated(): string
  {
    return $this->created->format(\DateTimeInterface::RFC3339);
  }

  /**
   * Creates with current time
   */
  public function setCreated(): self
  {
    $this->created = new \DateTime();

    return $this;
  }

  public function getBody(): array
  {
    return [
      "processid" => $this->getProcessId(),
      "name" => $this->getName(),
      "type" => $this->getType(),
      "runid" => $this->getRunId(),
      "message" => json_encode($this->getMessage()),
      "service" => $this->getService(),
      "createdon" => $this->getCreated()
    ];
  }
}
