<?php
declare(strict_types = 1);

namespace Drupal\robolytix\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\robolytix\ProcessType\ProcessType;
use Drupal\robolytix\Services\RobolytixApi;
use Drupal\robolytix\SonarType\SonarType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Robolytix settings for this site.
 */
class RobolytixAdminSettingsForm extends ConfigFormBase
{
  /**
   * @var MessengerInterface $messenger
   */
  protected $messenger;

  /**
   * @var RobolytixApi $robolytix
   */
  protected $robolytix;

  /**
   * @var EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var ModuleHandler $moduleHandler
   */
  protected $moduleHandler;

  /**
   * @var EntityFieldManagerInterface $entityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Class constructor.
   * @param RobolytixApi $robolytix
   * @param ConfigFactoryInterface $configFactory
   * @param MessengerInterface $messenger
   * @param EntityTypeManagerInterface $entityTypeManager
   * @param ModuleHandler $moduleHandler
   * @param EntityFieldManagerInterface $entityFieldManager
   */
  public function __construct(RobolytixApi $robolytix,
                              ConfigFactoryInterface $configFactory,
                              MessengerInterface $messenger,
                              EntityTypeManagerInterface $entityTypeManager,
                              ModuleHandler $moduleHandler,
                              EntityFieldManagerInterface $entityFieldManager
  )
  {
    parent::__construct($configFactory);
    $this->robolytix = $robolytix;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
    $this->moduleHandler = $moduleHandler;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('robolytix.robolytix_api'),
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'robolytix_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['robolytix.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('robolytix.settings');

    // First load
    if(!$form_state->getTriggeringElement()) {
      $this->removeTempSonars($config);
    }

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $form['general']['robolytix_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Robolytix Integration'),
      '#default_value' => $config->get('robolytix_enable'),
      '#description' => $this->t("If you want to integrate Robolytix sonars to your system."),
      '#required' => FALSE,
    ];

    $form['general']['robolytix_auth_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Robolytix API key'),
      '#default_value' => $config->get('robolytix_auth_token'),
      '#states' => [
        'visible' => [
          ':input[name="robolytix_enable"]' => ['checked' => TRUE],
        ],
      ],
      '#size' => 60,
      '#description' => $this->t("API key from your Robolytix."),
      '#required' => TRUE,
    ];

    $form['general']['robolytix_process_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Process type'),
      '#default_value' => $config->get('robolytix_process_type'),
      '#states' => [
        'visible' => [
          ':input[name="robolytix_enable"]' => ['checked' => TRUE],
        ],
      ],
      '#options' => [
        ProcessType::PROCESS_NOW => $this->t('Directly'),
        ProcessType::PROCESS_IN_QUEUE => $this->t('Process in background (recommended)'),
      ],
      '#description' => $this->t("You need set up CRON job for background processing."),
    ];

    $form['sonars'] = [
      '#type' => 'details',
      '#title' => $this->t('Sonars'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="robolytix_enable"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $nodeTypes = [];
    try {
      $nodeTypes = $this->getNodeTypes();
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
    }
    $nodeTypeFields = [];
    foreach(array_keys($nodeTypes) as $nodeType) {
      $fieldNames = array_keys($this->entityFieldManager->getFieldDefinitions('node', $nodeType));
      $fields = array_combine($fieldNames, $fieldNames);
      $nodeTypeFields[$nodeType] = $fields;
    }

    if (is_array($config->get('robolytix_sonar_ids'))) {
      foreach ($config->get('robolytix_sonar_ids') as $sonarId => $sonarValues) {
        if($sonarValues['temp_sonar_deleted']) {
          continue;
        }
        $form['sonars'][$sonarId] = [
          '#type' => 'details',
          '#title' => $this->t('Sonars for entity'),
          '#open' => FALSE,
          '#prefix' => '<div id="robolytix-sonar-wrapper-' . $sonarId . '">',
          '#suffix' => '</div>',
        ];
        $form['sonars'][$sonarId]['sonar_entity_' . $sonarId] = [
          '#type' => 'select',
          '#name' => 'sonar_entity_'. $sonarId,
          '#title' => $this->t('Select content type'),
          '#default_value' => $sonarValues['sonar_entity'],
          '#options' => $nodeTypes,
          '#description' => $this->t("The process of the entity will be monitored."),
          '#required' => TRUE,
        ];

        $form['sonars'][$sonarId]['sonar_process_id_' . $sonarId]  = [
          '#type' => 'textfield',
          '#title' => $this->t('Process ID'),
          '#default_value' => $sonarValues['sonar_process_id'],
          '#size' => 60,
          '#description' => $this->t("Process ID from your Robolytix."),
          '#required' => TRUE,
        ];

        $form['sonars'][$sonarId]['sonar_actions_'. $sonarId] = [
          '#title' => t('Sonars'),
          '#default_value' => $sonarValues['sonar_actions'],
          '#type' => 'checkboxes',
          '#description' => t('Select the sonars to be used.'),
          '#options' => $this->getSonarTypes(),
        ];

        foreach ($nodeTypeFields as $nodeType => $fields) {
          $defaultFields = array_intersect($sonarValues['sonar_fields'] === null ? [] : $sonarValues['sonar_fields'], $fields);
          $form['sonars'][$sonarId]['sonar_fields_'. $sonarId . '_' . $nodeType] = [
            '#title' => t('Custom fields'),
            '#default_value' => $defaultFields,
            '#type' => 'checkboxes',
            '#description' => t('Select the fields to be used.'),
            '#options' => $fields,
            '#prefix' => '<div id="robolytix-sonar-fields-' . $sonarId . '">',
            '#suffix' => '</div>',
            '#states' => [
              'visible' => [
                ':input[name="sonar_entity_' . $sonarId . '"]' => ['value' => $nodeType],
              ],
            ],
          ];
        }

        $form['sonars'][$sonarId]['sonar_remove'. $sonarId] = [
          '#type' => 'submit',
          '#name' => 'sonar_remove_'. $sonarId,
          '#value' => $this->t('Remove'),
          '#submit' => ['::removeSonarCallback'],
          '#limit_validation_errors' => [],
          '#ajax' => [
            'callback' => '::removeSonarAjax',
            'wrapper' => 'robolytix-sonar-wrapper-' . $sonarId,
            'method' => 'replace',
            'effect' => 'fade'
          ],
        ];
      }
    }

    $form['sonars']['add_more'] = [
      '#type' => 'submit',
      '#name' => 'add_more',
      '#value' => $this->t('Add another sonar'),
      '#submit' => ['::addSonarCallback'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::addSonarAjax',
        'wrapper' => 'robolytix-add-more-wrapper', // what is updated by ajax
        'effect' => 'fade',
        'method' => 'before'
      ],
      '#prefix' => '<div id="robolytix-add-more-wrapper">',
      '#suffix' => '</div>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Remove temporary sonars that were not saved
   * @param Config $config
   */
  protected function removeTempSonars(Config $config)
  {
    $sonarIds = $config->get('robolytix_sonar_ids');
    if (is_array($sonarIds)) {
      foreach ($sonarIds as $sonarId => $sonarValues) {
        if($sonarValues['temp_sonar_added']) {
          unset($sonarIds[$sonarId]);
        } elseif ($sonarValues['temp_sonar_deleted']) {
          $sonarIds[$sonarId]['temp_sonar_deleted'] = false;
        }
      }
    }
    $config->set('robolytix_sonar_ids', $sonarIds)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] === 'add_more' or
      substr( $form_state->getTriggeringElement()['#name'], 0, 12) === 'sonar_remove'
    ) {
      return;
    }

    parent::validateForm($form, $form_state);

    if(!$authToken = $form_state->getValue('robolytix_auth_token')) {
      $form_state->setErrorByName('robolytix_auth_token', $this->t('No token.'));
    }
    elseif($form_state->getValue('robolytix_auth_token') !== $form['general']['robolytix_auth_token']['#default_value']) {
      $response = $this->robolytix->pingApi($authToken);
      if ($response == null or $response->getStatusCode() >= 300) {
        $form_state->setErrorByName('robolytix_auth_token', $this->t('Wrong token.'));
      } else {
        $this->messenger->addStatus(t('Connection works well.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#name'] === 'add_more' or
      substr( $form_state->getTriggeringElement()['#name'], 0, 12) === 'sonar_remove'
    ) {
      return;
    }
    $config = $this->config('robolytix.settings');
    $config
      ->set('robolytix_enable', $form_state->getValue('robolytix_enable'))
      ->set('robolytix_auth_token', $form_state->getValue('robolytix_auth_token'))
      ->set('robolytix_process_type', $form_state->getValue('robolytix_process_type'))
      ->save();

    $sonarIds = $config->get('robolytix_sonar_ids');
    if (is_array($sonarIds)) {
      foreach ($sonarIds as $sonarId => $sonarValues) {
        if ($sonarValues['temp_sonar_deleted']) {
          unset($sonarIds[$sonarId]);
        } else {
          $sonarIds[$sonarId]['temp_sonar_added'] = false;
          $sonarEntity  = $form_state->getValue('sonar_entity_' . $sonarId);
          $sonarIds[$sonarId]['sonar_entity'] = $sonarEntity;
          if ($sonarEntity) {
            $sonarIds[$sonarId]['sonar_fields'] = $form_state->getValue('sonar_fields_' . $sonarId . '_' . $sonarEntity);
          }
          $sonarIds[$sonarId]['sonar_process_id'] = $form_state->getValue('sonar_process_id_' . $sonarId);
          $sonarIds[$sonarId]['sonar_actions'] = $form_state->getValue('sonar_actions_' . $sonarId);
        }
      }
    }
    $config->set('robolytix_sonar_ids', $sonarIds)->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Callback for add another sonar button.
   *
   * Selects and returns the fieldset with new row.
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function addSonarAjax(array &$form, FormStateInterface $form_state): array
  {
    $config = $this->config('robolytix.settings');
    $sonarIds = $config->get('robolytix_sonar_ids');
    $newSonarNumber = max(array_keys($sonarIds));
    return $form['sonars'][$newSonarNumber];
  }

  /**
   * Callback for add another sonar button.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function addSonarCallback(array &$form, FormStateInterface $form_state): void
  {
    $config = $this->config('robolytix.settings');
    $sonarIds = $config->get('robolytix_sonar_ids');
    if (is_array($sonarIds) and count($sonarIds) > 0) {
      $newSonarNumber = max(array_keys($sonarIds)) + 1;
      $sonarIds[$newSonarNumber] = [
        'sonar_entity' => null,
        'sonar_fields' => null,
        'sonar_process_id' => null,
        'sonar_actions' => [],
        'temp_sonar_added' => true,
        'temp_sonar_deleted' => false
      ];
    }
    else {
      $sonarIds = [
        0 => [
          'sonar_entity' => null,
          'sonar_fields' => null,
          'sonar_process_id' => null,
          'sonar_actions' => [],
          'temp_sonar_added' => true,
          'temp_sonar_deleted' => false
        ]
      ];
    }
    $config->set('robolytix_sonar_ids', $sonarIds)->save();
    $form_state->setRebuild();
  }

  /**
   * Callback for remove sonar button.
   *
   * Return empty array.
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array
   */
  public function removeSonarAjax(array &$form, FormStateInterface $form_state): array
  {
    return [null];
  }

  /**
   * Callback for remove sonar button.
   *
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function removeSonarCallback(array &$form, FormStateInterface $form_state): void
  {
    $removedSonarNumber = (int) substr($form_state->getTriggeringElement()['#name'], 13);
    $config = $this->config('robolytix.settings');
    $sonarIds = $config->get('robolytix_sonar_ids');
    if (array_key_exists($removedSonarNumber, $sonarIds)) {
      $sonarIds[$removedSonarNumber]['temp_sonar_deleted'] = true;
    }
    $config->set('robolytix_sonar_ids', $sonarIds)->save();
    $form_state->setRebuild();
  }

  /**
   * Get all node types.
   * @return array
   * @throws InvalidPluginDefinitionException
   * @throws PluginNotFoundException
   */
  protected function getNodeTypes(): array
  {
    $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $contentTypesList = [];
    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }
    return $contentTypesList;
  }

  /**
   * Is content moderation enabled?
   * @return bool
   */
  protected function isContentModerationEnabled(): bool
  {
    if ($this->moduleHandler->moduleExists('content_moderation')) {
      return true;
    }
    return false;
  }

  /**
   * Get sonar types
   * @return array
   */
  protected function getSonarTypes(): array
  {
    # the options to display in our checkboxes
    $sonarTypes = [
      SonarType::CREATE => $this->t('Create'),
      SonarType::UPDATE => $this->t('Update'),
      SonarType::DELETE => $this->t('Delete'),
    ];

    if ($this->isContentModerationEnabled()) {
      $sonarTypes = array_merge($sonarTypes,[
        SonarType::MODERATION_CREATE => $this->t('{Moderation} Create'),
        SonarType::MODERATION_UPDATE => $this->t('{Moderation} Update'),
        SonarType::MODERATION_DELETE => $this->t('{Moderation} Delete'),
      ]);
    }

    return $sonarTypes;
  }

}
